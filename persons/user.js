const express = require("express");
const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({extended: false});
global.user = {}
const app = express();
const userRouter = express.Router()

userRouter.delete("/:id", urlencodedParser, function(request, response){
    response.send("<h1>Удаление пользователя</h1><p>id=" + request.params.id +"</p>")
});

userRouter.get("/:id", urlencodedParser, function(request, response){
    response.send("<h1>Получение данных пользователя</h1><p>id=" + global.user.id+"</p>"+"<p> Name = "+global.user.name + "</p>");
});

userRouter.post("/create", urlencodedParser, function(request, response){
    global.user = request.body;
    global.user = request.body;
    response.send("<h1>Данные внесены</h1><p>id = "+global.user.id+"</p>"+"<p> Name = "+global.user.name + "</p>");
});

userRouter.put("/:id", urlencodedParser, function(request, response){
    global.user = request.body;
    global.user = request.body;
    response.send("<h1>Обновление пользователя с</h1><p>id = "+global.user.id);
});

 module.exports = userRouter;
