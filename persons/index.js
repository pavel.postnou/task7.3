const userRouter = require("./user.js");
const admRouter = require("./admin.js");
const fWRouter = require("./fworker.js");

module.exports = {
    userRouter,admRouter,fWRouter
}