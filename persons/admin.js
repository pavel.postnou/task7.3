const express = require("express");
const admParser = express.json();
global.admin = {}
const admRouter = express.Router();

admRouter.post("/create",admParser, function(request, response){
    global.admin.id = request.body;
    global.admin.name = request.body;
    response.send("<h1>Данные внесены</h1><p>id = "+global.admin.id+"</p>"+"<p> Name = "+global.admin.name + "</p>");
});

admRouter.get("/:id",admParser, function(request, response){
    response.send("<h1>id админа</h1><p>id=" + global.admin.id);
});

admRouter.put("/:id",admParser, function(request, response){
    response.send("<h1>Обновление данных админа</h1><p>id=" + global.admin.id);
});

admRouter.delete("/:id", admParser, function(request, response){
    response.send("<h1>Удаление админа</h1><p>id=" + global.admin.id);
});
 
module.exports = admRouter;