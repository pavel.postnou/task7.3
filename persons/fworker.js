const express = require("express");
let fs = require("fs");
const fWRouter = express.Router();
const multer = require('multer');
const { EventEmitter } = require("stream");
const upload = multer({dest:"uploads"});
const emitter = require("../emitter.js")


fWRouter.get("/writeSimple", function(request, response){
    fs.writeFileSync("hello.txt", "функция записи")
    response.send("<h1>Файл записан</h1>");
});

fWRouter.get("/readSimple", function(request, response){
    response.send("<h1>Содержимое файла: </h1><p>" + fs.readFileSync("hello.txt", "utf8"));
});

fWRouter.get("/writeStream", function(request, response){
    let writeableStream = fs.createWriteStream("hello.txt");
    writeableStream.write("Привет мир!");
    writeableStream.end;
    response.send("<h1>Данные внесены</h1>");
});
fWRouter.get("/readStream", function(request, response){
   let readableStream = fs.createReadStream("hello.txt", "utf8")
   readableStream.on("data", function(chunk){ 
    response.send("<h1>Данные файла из потока </h1>"+chunk);
});

});

fWRouter.post("/writeFile", upload.single ("file"), function(request, response){
    let fileData = request.file;
    emitter.emit("changer",fileData)
    response.send("Успешно")
});
 
module.exports = fWRouter;


